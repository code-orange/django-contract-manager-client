import requests
import os


def contract_manager_url():
    try:
        from django.conf import settings
    except ImportError:
        return os.getenv(
            "CONTRACT_MANAGER_API_URL",
            default="https://contract-manager-api.example.com/",
        )

    return settings.CONTRACT_MANAGER_API_URL


def contract_manager_default_credentials():
    try:
        from django.conf import settings
    except ImportError:
        username = os.getenv("CONTRACT_MANAGER_API_USER", default="user")
        password = os.getenv("CONTRACT_MANAGER_API_PASSWD", default="secret")
        return username, password

    username = settings.CONTRACT_MANAGER_API_USER
    password = settings.CONTRACT_MANAGER_API_PASSWD

    return username, password


def contract_manager_head(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.head(
        f"{contract_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.headers


def contract_manager_get_data(
    endpoint: str, api_user: str, api_password: str, params=None
):
    if params is None:
        params = dict()

    response = requests.get(
        f"{contract_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.json()


def contract_manager_post_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        f"{contract_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def contract_manager_patch_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{contract_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def contract_manager_put_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{contract_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def contract_manager_delete_data(
    endpoint: str, api_user: str, api_password: str, params=None
):
    if params is None:
        params = dict()

    response = requests.delete(
        f"{contract_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    if response.status_code == 200:
        return True

    return False


def contract_manager_get_api_credentials_for_customer(mdat_id: int):
    username, password = contract_manager_default_credentials()

    if username != mdat_id:
        response = contract_manager_get_data(
            f"v1/apiauthcustomerapikey/{mdat_id}",
            username,
            password,
        )

        username = str(response["user"]["id"])
        password = response["key"]

    return username, password


def contract_manager_get_tenant(mdat_id: int):
    username, password = contract_manager_get_api_credentials_for_customer(mdat_id)

    response = contract_manager_get_data(
        f"v1/tenant/{mdat_id}",
        username,
        password,
    )

    return response
